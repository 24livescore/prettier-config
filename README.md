# `@scorestats/prettier-config`

> Developement [Prettier](https://prettier.io) config.

## Usage

**Install**:

```bash
$  npm install --save-dev @scorestats/prettier-config
```

**Edit `package.json`**:

```jsonc
{
  // ...
  "prettier": "@scorestats/prettier-config"
}
```
